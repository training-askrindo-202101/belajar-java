# Belajar Java

Ketentuan menulis kode program Java

* Case sensitive
* Source code disimpan di file dengan extension `.java`
* Satu file bisa menyimpan banyak class, tapi hanya boleh ada satu public class dalam satu file
* Nama file harus sama dengan nama public class

## Compile dan Run ##

1. Compile source code

    ```
    javac NamaFile.java
    ```

    Melakukan kompilasi untuk semua file di folder `src`, hasil kompilasi ditaruh di folder `bin`

    ```
    javac src/*.java -d bin
    ```

2. Menjalankan kode program Java

    ```
    java NamaClass
    ```

    Class yang dijalankan harus punya method `public static void main (String[] x)`

    Menjalankan class dengan nama lengkap

    ```
    java nama.package.lengkap.NamaClass
    ```

## CLASSPATH ##

Classpath : lokasi tempat menemukan class-class yang akan dipakai.

Cara setting classpath : menambahkan variabel `CLASSPATH` di environment variable.

* Windows

    * Perintah di command line : `set CLASSPATH=lokasi-foldernya`. Hanya berlaku di command prompt tempat kita ketik.
    * Set di Advanced Properties : berlaku global dan permanen.

* *Nix

    * Perintah di command line : `export CLASSPATH=lokasi-foldernya`. Berlaku di command prompt tersebut
    * Edit file `.bashrc`, `.zshrc`, dsb tergantung shell yang dipakai. Berlaku di user tersebut
    * Ubuntu : set di `/etc/environment`. Berlaku global untuk semua user

## Packaging ##

Class-class yang kita buat, dipaketkan dalam file `jar`. Cara membuat file jar :

1. Compress folder berisi package dan class dengan metode `zip`. Apabila menggunakan aplikasi `WinRAR`, pilih kompresi `zip`, jangan pilih `rar`.

    [![Compress](img/01-compress.png)](img/01-compress.png)

2. Rename file

    [![Rename](img/02-rename.png)](img/02-rename.png)

    Ganti extension file dari `zip` menjadi `jar`

    [![Zip to Jar](img/03-zip-to-jar.png)](img/03-zip-to-jar.png)

    [![Confirm](img/04-confirm.png)](img/03-confirm.png)

    [![Finish](img/05-finish.png)](img/05-finish.png)

3. Cek isi file `jar` dengan perintah berikut

    ```
    jar -tvf namafile.jar
    ```

    Outputnya seperti ini

    ```
     0 Thu Dec 09 11:02:22 WIB 2021 com/
     0 Thu Dec 09 11:02:22 WIB 2021 com/muhardin/
     0 Thu Dec 09 11:02:22 WIB 2021 com/muhardin/endy/
     0 Thu Dec 09 11:02:22 WIB 2021 com/muhardin/endy/belajar/
     0 Thu Dec 09 11:02:22 WIB 2021 com/muhardin/endy/belajar/java/
   442 Thu Dec 09 11:02:22 WIB 2021 com/muhardin/endy/belajar/java/Halo.class
   444 Thu Dec 09 11:02:22 WIB 2021 Customer.class
    ```

4. File jar bisa ditambahkan ke dalam `CLASSPATH`

    ```
    export CLASSPATH=/lokasi/folder/file.jar
    ```

5. Selanjutnya, class bisa dipanggil seperti biasa

    ```
    java com.muhardin.endy.belajar.java.Halo
    ```